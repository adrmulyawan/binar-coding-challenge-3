const express = require('express');
const router = express.Router();
const {
  login,
  loginProcess,
  register,
  logout,
  registerProcess
} = require('../controllers/auth.controller');
let Users = require('../data/users.json');
const { body, validationResult, check } = require('express-validator');

router.route('/login')
  .get(login)
  .post(loginProcess);

router.get('/logout', logout);

router.route('/register')
  .get(register)
  .post([
    body('username').custom((value) => {
      const duplicate = Users.find((user) => user.username === value);
      if (duplicate) {
        throw new Error('Username is Registered!');
      }
      return true;
    }),
    body('email').custom((value) => {
      const duplicate = Users.find((user) => user.email === value);
      if (duplicate) {
        throw new Error('Email is Registered!');
      }
      return true;
    }),
    check('password', 'Minimum 6 Character Password').isLength({
      min: 6
    })
  ], registerProcess);

module.exports = router;