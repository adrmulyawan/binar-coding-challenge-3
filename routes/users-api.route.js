const express = require('express');
const router = express.Router();
const { body, validationResult, check } = require('express-validator');
let Users = require('../data/users.json');
const { 
  login, 
  getAllDataUsers, 
  register,
  getDetailUser,
  update,
  deleteDataUser
} = require('../controllers/users-api.controller')

router.get('/api/v1/users', getAllDataUsers);

router.get('/api/v1/users/:id', getDetailUser);

router.post('/api/v1/users/login', login);

router.post('/api/v1/users/register',[
  body('username').custom((value) => {
    const duplicate = Users.find(user => user.username === value);
    if (duplicate) {
      throw new Error('Username has been used!');
    }
    return true;
  }),
  body('email').custom((value) => {
    const duplicate = Users.find(user => user.email === value);
    if (duplicate) {
      throw new Error('Email has been used!');
    }
    return true
  }),
  check('password', 'Minimum 6 Character Password').isLength({
    min: 6
  })
], register);

router.put('/api/v1/users/update/:id', [
  body('username').custom((value, {req}) => {
    const duplicate = Users.find(user => user.username === value);
    if (value !== req.body.username && duplicate) {
      throw new Error('Username has been used!');
    }
    return true;
  }),
  body('email').custom((value, {req}) => {
    const duplicate = Users.find(user => user.email === value);
    if (value !== req.body.email && duplicate) {
      throw new Error('Email has been used!');
    }
    return true
  }),
  check('password', 'Minimum 6 Character Password').isLength({
    min: 6
  })
], update)

router.delete('/api/v1/users/delete/:id', deleteDataUser);

module.exports = router;