const { validationResult } = require('express-validator');
const { v4: uuidv4 } = require('uuid');
let Users = require('../data/users.json');

let session;

const login = (req, res) => {
  res.render('pages/login', {
    layout: 'layouts/auth-layouts',
    title: 'Login',
    message: req.flash('message'),
    success: req.flash('success')
  });
};

const loginProcess = (req, res) => {
  // > Catch User Email and Password
  const {username, password} = req.body;
  
  // > Search in Users.json 'username' is it the same as the one sent in the form by the user 
  const person = Users.find(user => {
    return user.username === username
  });
  
  // > Check username is match
  if (person) {
    // > if username and password match
    if (person.username == username && person.password == password) {
      session = req.session;
      session.userid = person.id;
      session.username = username;
      console.info(session);
      res.redirect('/');
    }
    // > if email and password dont match
    else {
      req.flash('message', 'Wrong Password!')
      res.redirect('/login');
    }
  } else {
    req.flash('message', 'Username is Not Found!');
    res.redirect('/login');
  }
};

const register = (req, res) => {
  res.render('pages/register', {
    layout: 'layouts/auth-layouts',
    title: 'Register',
    message: req.flash('message')
  });
};

const registerProcess = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.render('pages/register', {
      layout: 'layouts/auth-layouts',
      title: 'Register',
      errors: errors.array()
    });
  } else {
    const {username, email, password} = req.body;

    // > Create new object
    const newUser = {
      id: uuidv4(),
      username: username,
      email: email,
      password: password
    };

    Users.push(newUser);

    req.flash('success', 'Success Register Your Account!');
    res.redirect('/login');
  }
};

const logout = (req, res) => {
  req.session.destroy();
  res.redirect('/login');
};

module.exports = {
  login,
  loginProcess,
  register,
  registerProcess,
  logout
}