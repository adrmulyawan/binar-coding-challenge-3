const { validationResult } = require('express-validator');
const { v4: uuidv4 } = require('uuid');
let Users = require('../data/users.json');

const getAllDataUsers = (req, res) => {
  if (Users.length < 1) {
    res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'sorry user data is still empty!'
    });
  } else {
    res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data users found',
      data: Users
    });
  }
};

const getDetailUser = (req, res) => {
  const idUser = req.params.id;

  const found = Users.find(user => user.id == idUser);
  console.info(found);

  if (found) {
    res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: 'Data Found!',
      data: found
    });
  } else {
    res.status(404).json({
      status: 'Failed',
      statusCode: 404,
      message: `User with id = ${idUser} not found!`
    });
  }
};

const login = (req, res) => {
  const {username, password} = req.body;

  const person = Users.find(user => user.username === username);

  if (person) {
    if (person.username === username && person.password === password) {
      res.status(200).json({
        status: 'Success',
        statusCode: 200,
        message: `Hello ${person.username}, login successfully!`
      });
    } else {
      res.status(401).json({
        status: 'Failed',
        statusCode: 401,
        message: 'Wrong Password!'
      });
    }
  } else {
    res.status(404).json({
      status: 'Failed',
      statusCode: 404,
      message: `Sorry username "${username}" not found!`
    });
  }
};

const register = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: 'Oops! something error',
      errors: errors.array()
    });
  } else {
    const {username, email, password} = req.body;

    // > Create new object
    const newUser = {
      id: uuidv4(),
      username: username,
      email: email,
      password: password
    };

    Users.push(newUser);

    res.status(201).json({
      status: 'Success',
      statusCode: 201,
      message: 'Success register new user',
      data: newUser,
      users: Users
    });
  }
};

const update = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.status(400).json({
      status: 'Failed',
      statusCode: 400,
      message: 'Oops! something error',
      errors: errors.array()
    });
  } else {
    let dataUser = Users.find(user => user.id == req.params.id);

    const params = {
      username: req.body.username,
      email: req.body.email,
      password: req.body.password
    };

    dataUser = {...dataUser, ...params};

    Users = Users.map(user => {
      return user.id == dataUser.id ? dataUser : user;
    })

    res.status(201).json({
      status: 'Success',
      statusCode: 201,
      message: 'Success update data user',
      data: dataUser,
      users: Users
    });
  }
};

const deleteDataUser = (req, res) => {
  const dataUser = Users.find(user => user.id == req.params.id);

  if (!dataUser) {
    res.status(404).json({
      status: 'Failed',
      statusCode: 404,
      message: `User with id = ${req.params.id} not found!`
    });
  } else {
    Users = Users.filter(user => user.id != req.params.id);
    res.status(200).json({
      status: 'Success',
      statusCode: 200,
      message: `Success delete data user with id = ${req.params.id}`,
      users: Users
    });
  }
};

module.exports = {
  login,
  getAllDataUsers,
  register,
  getDetailUser,
  update,
  deleteDataUser,
};